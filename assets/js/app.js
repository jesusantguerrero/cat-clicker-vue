const levels = ["Pino", "Guayabo", "Caoba", "Roble", "Roble Blanco", "Roble Dorado", "Roble Platino"]

const cats = [{
    id: 1,
    name: "Herbie",
    picture: './assets/img/cat1.jpg',
    clickCount: 0,
    nicknames: ["cosita"],
    level: 'pino'
  },
  {
    id: 2,
    name: "Sheercat",
    picture: './assets/img/cat2.jpg',
    clickCount: 0,
    nicknames: ["The style"],
    level: 'pino'
  },
  {
    id: 3,
    name: "The Twins",
    picture: './assets/img/cat3.jpg',
    clickCount: 0,
    nicknames: ["The twinas"],
    level: 'pino'
  },
  {
    id: 4,
    name: "Bore",
    picture: './assets/img/cat4.jpg',
    clickCount: 0,
    nicknames: ["Boringcat"],
    level: 'pino'
  },
  {
    id: 5,
    name: "Jacky",
    picture: './assets/img/cat5.jpg',
    clickCount: 0,
    nicknames: ["Ninja Cat"],
    level: 'pino'
  }
]

const catListView = new Vue({
  el: '#cat-list',
  data: {
    cats: cats
  },
  methods: {
    changeCat: function(id){
      catView.cat = cats[id - 1]
    }
  }
})

const catView = new Vue({
  el: '#cat',
  data: {
    cat: cats[0]
  },
  methods: {
    incrementCounter: function () {
      catView.cat.clickCount += 1;
      var clickCount = catView.cat.clickCount
      if ((clickCount % 10) == 0) {
        var index = (clickCount / 10)
        if (index < 7)
          catView.cat.level = levels[index]
      }
    }
  }
})